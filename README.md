MultiCraft_game Open Source
===========================

MultiCraft_game is an Open Source module for MultiCraft ― sandbox game inspired by [Minecraft](https://minecraft.net/).

MultiCraft_game is based on the minetest_game, which is developed by a [number of contributors](https://github.com/minetest/minetest_game/graphs/contributors).
It also uses many mods from various developers with various licenses. Carefully read the README and LICENSE files in each folder.

Copyright © 2014-2019 Maksim Gamarnik [MoNTE48] <MoNTE48@mail.ua> & MultiCraft Development Team.

MultiCraft_game is licensed under LGPLv3 (or higher). Resources games licensed under CC-BY-SA 4.0, unless otherwise stated.

You can help in the development. But you should always publish your source code after any changes.
Before any use of the MultiCraft_game source code or any part of it, you should be familiar with [LGPL-3.0](doc/LGPL-3.0.md) and [Other License.md](doc/License.txt)

Any code that you push to this repository is automatically licensed as LGPLv3 (or higher) and belongs MultiCraft Project and/or the owner of the project without exception.
Adding code under another license is possible only by agreement with the project owner and the creation of appropriate notes.